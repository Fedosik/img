#ifndef IMG__PYTHON_IMG_CPP_
#define IMG__PYTHON_IMG_CPP_

#include <python3.6/Python.h>
#include "img.h"

#define CHECK_IMG_RETURN_VALUE(value) if (value) return NULL

enum InputType : char {
  Binarization = 0,
  EdgeDetection = 1
};

int depth;
bool blur;
float scalar;

int ParseInputToRgbImage(PyObject* args, img::RgbImage* res, InputType input_type) {
  int n, m;
  PyObject* pixels;
  switch (input_type) {
    case InputType::Binarization: {
      if (!PyArg_ParseTuple(args, "iiOi", &n, &m, &pixels, &depth)) {
        return 1;
      }
      break;
    }
    case InputType::EdgeDetection: {
      if (!PyArg_ParseTuple(args, "iiOpf", &n, &m, &pixels, &blur, &scalar)) {
        return 1;
      }
      break;
    }
    default: {
      return 228;
    }
  }
  *res = img::RgbImage(n, m);
  if (!PyList_Check(pixels) || n <= 0 || m <= 0) {
    return 2;
  }
  if (PyList_Size(pixels) != n * m) {
    return 42;
  }
  for (int i = 0; i < PyList_Size(pixels); i++) {
    auto pixel = PyList_GetItem(pixels, i);
    if (!PyTuple_Check(pixel)) {
      return 3;
    }
    int r, g, b;
    if (!PyArg_ParseTuple(pixel, "iii", &r, &g, &b)) {
      return 4;
    }
    int row = i / m;
    int column = i % m;
    (*res)[row][column] = {r, g, b};
  }
  return 0;
}

PyObject* ParseRgbImageToListOfTuples(const img::RgbImage& image) {
  PyObject* list = PyList_New(image.GetHeight() * image.GetWidth());
  for (int i = 0; i < PyList_Size(list); i++) {
    int row = i / image.GetWidth();
    int column = i % image.GetWidth();
    PyList_SetItem(list, i,  Py_BuildValue(("iii"), image[row][column].r, image[row][column].g, image[row][column].b));
  }
  return list;
}

static PyObject* OtsuBinarize(PyObject* self, PyObject* args) {
  img::RgbImage image;
  CHECK_IMG_RETURN_VALUE(ParseInputToRgbImage(args, &image, InputType::Binarization));
  return ParseRgbImageToListOfTuples(img::BinarizeImage(image, img::CalcOtsuThreshold, depth));
}

static PyObject* Otsu2dBinarize(PyObject* self, PyObject* args) {
  img::RgbImage image;
  CHECK_IMG_RETURN_VALUE(ParseInputToRgbImage(args, &image, InputType::Binarization));
  return ParseRgbImageToListOfTuples(img::BinarizeImage(image, img::Calc2dOtsuThreshold, depth));
}


static PyObject* CannyEdgeDetection(PyObject* self, PyObject* args) {
  img::RgbImage image;
  CHECK_IMG_RETURN_VALUE(ParseInputToRgbImage(args, &image, InputType::EdgeDetection));
  return ParseRgbImageToListOfTuples(img::SimpleEdgeDetection(image, img::Canny, img::Calc2dOtsuThreshold, scalar, blur));
}

static PyObject* SobelEdgeDetection(PyObject* self, PyObject* args) {
  img::RgbImage image;
  CHECK_IMG_RETURN_VALUE(ParseInputToRgbImage(args, &image, InputType::EdgeDetection));
  return ParseRgbImageToListOfTuples(img::SimpleEdgeDetection(image, img::Sobel, img::Calc2dOtsuThreshold, scalar, blur));
}

static PyObject* KirschEdgeDetection(PyObject* self, PyObject* args) {
  img::RgbImage image;
  CHECK_IMG_RETURN_VALUE(ParseInputToRgbImage(args, &image, InputType::EdgeDetection));
  return ParseRgbImageToListOfTuples(img::SimpleEdgeDetection(image, img::Kirsch, img::Calc2dOtsuThreshold, scalar, blur));
}

static PyMethodDef Methods[] = {
  {"OtsuBinarize", OtsuBinarize, METH_VARARGS, ""},
  {"Otsu2dBinarize", Otsu2dBinarize, METH_VARARGS, ""},
  {"CannyEdgeDetection", CannyEdgeDetection, METH_VARARGS, ""},
  {"SobelEdgeDetection", SobelEdgeDetection, METH_VARARGS, ""},
  {"KirschEdgeDetection", KirschEdgeDetection, METH_VARARGS, ""},
  {NULL, NULL, 0, NULL}
};


static struct PyModuleDef ImgModule = {
  PyModuleDef_HEAD_INIT,
  "ImgModule",     /* name of module */
  "",          /* module documentation, may be NULL */
  -1,          /* size of per-interpreter state of the module, or -1 if the module keeps state in global variables. */
  Methods
};

PyMODINIT_FUNC PyInit_ImgModule(void) {
  return PyModule_Create(&ImgModule);
}
//


#endif //IMG__PYTHON_IMG_CPP_
