import telebot
import img_lib
import os

bot = telebot.TeleBot(os.environ['TOKEN'])

help_text = '''
Возможные алгоритмы: canny, sobel, kirsch, otsu, otsu2d

Желаемые алгоритм нужно вписать в caption для фото. 

Параметры так же пишутся в caption.
  
Параметры для otsu, otsu2d:
1. depth=одно целое число.
  
Параметры для canny, sobel, kirsch:
1. scalar=одно вещественное число.
2. blur, если указан то будет применён фильтр гаусса перед обработкой.
'''

def IsInt(x):
  try:
    int(x)
  except Exception as e:
    return False
  else:
    return True


def IsFloat(x):
  try:
    float(x)
  except Exception as e:
    return False
  else:
    return True

def GetSubstr(descr: str, p):
  while ((p < len(descr)) and (descr[p] == ' ')):
    p += 1
  l = p
  while ((p < len(descr)) and (descr[p] != ' ')):
    p += 1
  r = p
  return descr[l:r]


def Bin(descr: str, foo, img: img_lib.Image):
  p = descr.find('depth=')
  depth = 1
  if p != -1:
    p += len('depth=')
    substr = GetSubstr(descr, p)
    if not IsInt(substr):
      return (1, None)
    depth = int(substr)
    if (depth <= 0) or (depth >= 10):
      return (1, None)
  return (0, foo(img, depth))


def Edg(descr: str, foo, img: img_lib.Image):
  p = descr.find('scalar=')
  scalar = 1
  if p != -1:
    p += len('scalar=')
    substr = GetSubstr(descr, p)
    if not IsFloat(substr):
      return (1, None)
    scalar = float(substr)
    if (scalar <= 0):
      return (1, None)
  blur = False
  if descr.find('blur'):
    blur = True
  return (0, foo(img, blur, scalar))


@bot.message_handler(commands=['help'])
def help(message):
  bot.send_message(message.chat.id, help_text)


@bot.message_handler(content_types=['photo'])
def Foo(message):
  print(message.from_user.username, message.caption)
  for item in message.photo:
    photo = bot.get_file(item.file_id)
    file = bot.download_file(photo.file_path)
    init = img_lib.GetImageFromBytes(file)
    if init is None:
      bot.send_message(message.chat.id, "can't read image")
      return
    if 'otsu' in message.caption:
      error, res = Bin(message.caption, img_lib.Otsu, init)
    elif 'otsu2d' in message.caption:
      error, res = Bin(message.caption, img_lib.Otsu2d, init)
    elif 'kirsch' in message.caption:
      error, res = Edg(message.caption, img_lib.Kirsch, init)
    elif 'sobel' in message.caption:
      error, res = Edg(message.caption, img_lib.Sobel, init)
    elif 'canny' in message.caption:
      error, res = Edg(message.caption, img_lib.Canny, init)
    else:
      bot.send_message(message.chat.id, "haven't found algorithm name to apply")
      return
    if error:
      bot.send_message(message.chat.id, "some parameters are set wrong")
    res.save('./answers/res.png')
    bot.send_photo(message.chat.id, open('./answers/res.png', 'rb'))


def main():
  try:
    bot.polling(none_stop=True)
  except Exception as e:
    print(e)

if __name__ == "__main__":
  main()
