#include <bits/stdc++.h>

namespace img {

template <typename T>
T sqr(T x) {
  return x * x;
}

void FitInEightBits(int& x) {
  x = std::min(std::max(x, 0), (1 << 8) - 1);
}

int FittedInEightBits(int x) {
  return std::min(std::max(x, 0), (1 << 8) - 1);
}


struct RgbPixel {
  int r = 0, g = 0, b = 0;

  RgbPixel() = default;

  RgbPixel(int r, int g, int b) : r(r), g(g), b(b) {
  }

  RgbPixel(const std::array<int, 3>& obj) : r(obj[0]), g(obj[1]), b(obj[2]) {
  }

  void FitInEightBits() {
    img::FitInEightBits(r);
    img::FitInEightBits(g);
    img::FitInEightBits(b);
  }

  RgbPixel& operator+=(const RgbPixel& obj) {
    r += obj.r;
    g += obj.g;
    b += obj.b;
    return *this;
  }

  RgbPixel& operator-=(const RgbPixel& obj) {
    r -= obj.r;
    g -= obj.g;
    b -= obj.b;
    return *this;
  }

  RgbPixel& operator*=(const int16_t& scalar) {
    r *= scalar;
    g *= scalar;
    b *= scalar;
    return *this;
  }
};

RgbPixel Flip(const RgbPixel& pixel) {
  return {255 - pixel.r, 255 - pixel.g, 255 - pixel.b};
}

static const auto kWhitePixel = RgbPixel(255, 255, 255);

static const auto kBlackPixel = RgbPixel(0, 0, 0);

RgbPixel operator+(const RgbPixel& a, const RgbPixel& b) {
  return {a.r + b.r, a.g + b.g, a.b + b.b};
}

RgbPixel operator-(const RgbPixel& a, const RgbPixel& b) {
  return {a.r - b.r, a.g - b.g, a.b - b.b};
}

RgbPixel operator*(const RgbPixel& a, int scalar) {
  return {a.r * scalar, a.g * scalar, a.b * scalar};
}

RgbPixel operator*(int scalar, const RgbPixel& a) {
  return a * scalar;
}

bool operator==(const RgbPixel& a, const RgbPixel& b) {
  return (a.r == b.r && a.g == b.g && a.b == b.b);
}

struct Point {
  int row = 0;
  int column = 0;

  Point() = default;

  Point(int row, int column) : row(row), column(column) {
  }
};

template <typename Element>
class Matrix {
 public:
  Matrix() : h_(0), w_(0) {
  }

  Matrix(int h, int w) : h_(h), w_(w) {
    matrix_.resize(h_);
    for (auto& row : matrix_) {
      row.resize(w_);
    }
  }

  explicit Matrix(const std::vector<std::vector<Element>>& obj) {
    assert(!obj.empty() && "should be empty");
    h_ = obj.size();
    w_ = obj[0].size();
    matrix_.resize(h_);
    for (int row = 0; row < h_; row++) {
      assert(obj[row].size() == w_ && "each row should have equal size");
      matrix_[row].resize(w_);
      for (int column = 0; column < w_; column++) {
        matrix_[row][column] = obj[row][column];
      }
    }
  }

  int GetHeight() const {
    return h_;
  }

  int GetWidth() const {
    return w_;
  }

  std::vector<Point> GetListOfIndices() const {
    std::vector<Point> res(h_ * w_);
    for (int row = 0; row < h_; row++) {
      for (int column = 0; column < w_; column++) {
        res[row * w_ + column] = Point(row, column);
      }
    }
    return res;
  }

  const std::vector<Element>& operator[](int index) const {
    return matrix_[index];
  }

  std::vector<Element>& operator[](int index) {
    return matrix_[index];
  }

  const Element& operator[](const Point& point) const {
    return matrix_[point.row][point.column];
  }

  Element& operator[](const Point& point) {
    return matrix_[point.row][point.column];
  }

 protected:
  int h_{};
  int w_{};

  std::vector<std::vector<Element>> matrix_;
};

using IntensityMatrix = Matrix<int>;

class RgbImage : public Matrix<RgbPixel> {
 public:
  RgbImage() = default;

  explicit RgbImage(int h, int w) : Matrix(h, w) {
  }

  explicit RgbImage(const std::vector<std::vector<std::array<int, 3>>>& obj) : Matrix(ConvertVVAToVVP(obj)) {
  }

  explicit RgbImage(const IntensityMatrix& obj) : Matrix(ConvertIntensityMatrixToVVP(obj)) {
  }

 private:
  static std::vector<std::vector<RgbPixel>> ConvertVVAToVVP(const std::vector<std::vector<std::array<int, 3>>>& obj) {
    std::vector<std::vector<RgbPixel>> v(obj.size());
    for (int row = 0; row < v.size(); row++) {
      v[row].resize(obj[row].size());
      for (int column = 0; column < obj[row].size(); column++) {
        v[row][column] = RgbPixel(obj[row][column]);
      }
    }
    return v;
  }

  static std::vector<std::vector<RgbPixel>> ConvertIntensityMatrixToVVP(const IntensityMatrix& obj) {
    std::vector<std::vector<RgbPixel>> v(obj.GetHeight());
    for (int row = 0; row < v.size(); row++) {
      v[row].resize(obj.GetWidth());
      for (int column = 0; column < obj.GetWidth(); column++) {
        v[row][column] = RgbPixel(obj[row][column], obj[row][column], obj[row][column]);
      }
    }
    return v;
  }
};

RgbImage MulpSize(const RgbImage& image, int c) {
  RgbImage res(image.GetHeight() * c, image.GetWidth() * c);
  for (auto point : image.GetListOfIndices()) {
    for (int i = 0; i < c; i++) {
      for (int j = 0; j < c; j++) {
        res[point.row * c + i][point.column * c + j] = image[point];
      }
    }
  }
  return res;
}

using IndicesList = std::vector<Point>;

IntensityMatrix GrayScale(const RgbImage& image) {
  IntensityMatrix matrix(image.GetHeight(), image.GetWidth());
  for (const auto& point : matrix.GetListOfIndices()) {
    matrix[point] = (image[point].r + image[point].g + image[point].b) / 3;
  }
  return matrix;
}

void GetDivision(
  const IntensityMatrix& matrix, const IndicesList& indices, int threshold, IndicesList* blacks, IndicesList* whites) {
  for (auto point : indices) {
    if (matrix[point] <= threshold) {
      blacks->emplace_back(point);
    } else {
      whites->emplace_back(point);
    }
  }
}

int CalcMedianThreshold(const IntensityMatrix& intensity_matrix, const IndicesList& indices) {
  std::vector<int> intensities;
  for (auto point : indices) {
    intensities.emplace_back(intensity_matrix[point]);
  }
  sort(intensities.begin(), intensities.end());
  int median;
  if (intensities.size() % 2 == 1) {
    median = intensities[intensities.size() / 2];
  } else {
    median = (intensities[intensities.size() / 2] + intensities[intensities.size() / 2 - 1]) / 2;
  }
  return median;
}

int CalcAverageThreshold(const IntensityMatrix& intensity_matrix, const IndicesList& indices) {
  int sum = 0;
  for (auto point : indices) {
    sum += intensity_matrix[point];
  }
  return sum / (intensity_matrix.GetWidth() * intensity_matrix.GetHeight());
}

using ConvolutionKernel = Matrix<int>;

IntensityMatrix ApplyConvolution(const IntensityMatrix& intensity_matrix, const ConvolutionKernel& kernel) {
  IntensityMatrix res(intensity_matrix.GetHeight(), intensity_matrix.GetWidth());
  for (auto point : intensity_matrix.GetListOfIndices()) {
    int midx = kernel.GetHeight() / 2;
    int midy = kernel.GetWidth() / 2;
    int sum = 0;
    for (auto dpoint : kernel.GetListOfIndices()) {
      int trow = point.row + dpoint.row - midx;
      int tcolumn = point.column + dpoint.column - midy;
      if (trow < 0 || trow >= intensity_matrix.GetHeight()) {
        continue;
      }
      if (tcolumn < 0 || tcolumn >= intensity_matrix.GetWidth()) {
        continue;
      }
      sum += kernel[dpoint] * intensity_matrix[trow][tcolumn];
    }
    res[point] = sum;
  }
  return res;
}

int CalcOtsuThreshold(const IntensityMatrix& intensity_matrix, const IndicesList& indices) {
  std::vector<int> hist(256);
  int total_intetnsity = 0;
  for (auto point : indices) {
    hist[FittedInEightBits(intensity_matrix[point])]++;
    total_intetnsity += FittedInEightBits(intensity_matrix[point]);
  }
  int threshold = 0;
  double best_sigma = 0;
  int black_pixels_cnt = 0;
  int black_pixels_intensity = 0;
  for (int i = 0; i < 256; i++) {
    black_pixels_cnt += hist[i];
    black_pixels_intensity += i * hist[i];
    if (black_pixels_cnt == 0 || black_pixels_cnt == indices.size()) {
      continue;
    }
    double black_pixels_prob = 1. * black_pixels_cnt / indices.size();
    double white_pixels_prob = 1 - black_pixels_prob;
    double black_pixels_mean = 1. * black_pixels_intensity / black_pixels_cnt;
    double white_pixels_mean =
      1. * (total_intetnsity - black_pixels_intensity) / (int(indices.size()) - black_pixels_cnt);
    double mean_delta = black_pixels_mean - white_pixels_mean;
    double sigma = mean_delta * mean_delta * white_pixels_prob * black_pixels_prob;
    if (sigma > best_sigma) {
      best_sigma = sigma;
      threshold = i;
    }
  }
  return threshold;
}

int Calc2dOtsuThreshold(const IntensityMatrix& intensity_matrix, const IndicesList& indices) {
  Matrix<double> hist(256, 256);
  for (auto point : indices) {
    int row = point.row;
    int column = point.column;
    int sum = 0;
    int cnt = 0;
    for (int di = -1; di <= 1; di++) {
      for (int dj = -1; dj <= 1; dj++) {
        if (di == 0 && dj == 0) {
          continue;
        }
        if (row + di < 0 || row + di >= intensity_matrix.GetHeight()) {
          continue;
        }
        if (column + dj < 0 || column + dj >= intensity_matrix.GetWidth()) {
          continue;
        }
        sum += FittedInEightBits(intensity_matrix[row + di][column + dj]);
        cnt++;
      }
    }
    hist[FittedInEightBits(intensity_matrix[row][column])][sum / cnt]++;
  }
  for (auto point : hist.GetListOfIndices()) {
    hist[point] *= 1. / indices.size();
  }
  double mu_t0 = 0;
  double mu_t1 = 0;
  for (int i = 0; i < 256; i++) {
    for (int j = 0; j < 256; j++) {
      mu_t1 += hist[i][j] * j;
      mu_t0 += hist[i][j] * i;
    }
  }
  Matrix<double> p0(256, 256);
  Matrix<double> mu_i(256, 256);
  Matrix<double> mu_j(256, 256);
  double mx = 0;
  int threshold = 0;
  for (int i = 0; i < 256; i++) {
    for (int j = 0; j < 256; j++) {
      p0[i][j] = hist[i][j];
      mu_i[i][j] = hist[i][j] * i;
      mu_j[i][j] = hist[i][j] * j;
      if (i > 0) {
        p0[i][j] += p0[i - 1][j];
        mu_i[i][j] += mu_i[i - 1][j];
        mu_j[i][j] += mu_j[i - 1][j];
      }
      if (j > 0) {
        p0[i][j] += p0[i][j - 1];
        mu_i[i][j] += mu_i[i][j - 1];
        mu_j[i][j] += mu_j[i][j - 1];
      }
      if (i > 0 && j > 0) {
        p0[i][j] -= p0[i - 1][j - 1];
        mu_i[i][j] -= mu_i[i - 1][j - 1];
        mu_j[i][j] -= mu_j[i - 1][j - 1];
      }
      if (p0[i][j] == 0) {
        continue;
      }
      if (p0[i][j] == 1) {
        continue;
      }
      double tr = (sqr(mu_i[i][j] - p0[i][j] * mu_t0) + sqr(mu_j[i][j] - p0[i][j] * mu_t1));
      tr /= (p0[i][j] * (1 - p0[i][j]));
      if (tr > mx) {
        mx = tr;
        threshold = i;
      }
    }
  }
  return threshold;
}

IntensityMatrix ApplyGaussBlur(const IntensityMatrix& intensityMatrix) {
  ConvolutionKernel gauss(5, 5);
  gauss[0] = {2, 4, 5, 4, 2};
  gauss[1] = {4, 9, 12, 9, 4};
  gauss[2] = {5, 12, 15, 12, 5};
  gauss[3] = {4, 9, 12, 9, 4};
  gauss[4] = {2, 4, 5, 4, 2};
  auto res = ApplyConvolution(intensityMatrix, gauss);
  for (auto point : res.GetListOfIndices()) {
    res[point] /= 160;
  }
  return res;
}

struct SobelRes {
  IntensityMatrix gradient;
  IntensityMatrix dx;
  IntensityMatrix dy;
};

struct KirschRes {
  IntensityMatrix gradient;
};

struct CannyRes {
  IntensityMatrix gradient;
};


SobelRes Sobel(const IntensityMatrix& intensity_matrix, double alpha) {
  ConvolutionKernel dx_kernel(3, 3), dy_kernel(3, 3);

  dx_kernel[0] = {-1, 0, 1};
  dx_kernel[1] = {-2, 0, 2};
  dx_kernel[2] = {-1, 0, 1};

  dy_kernel[0] = {-1, -2, -1};
  dy_kernel[1] = {0, 0, 0};
  dy_kernel[2] = {1, 2, 1};

  SobelRes res;
  res.dx = ApplyConvolution(intensity_matrix, dx_kernel);
  res.dy = ApplyConvolution(intensity_matrix, dy_kernel);
  res.gradient = IntensityMatrix(intensity_matrix.GetHeight(), intensity_matrix.GetWidth());

  for (auto point : intensity_matrix.GetListOfIndices()) {
    res.gradient[point] = static_cast<int>(alpha * sqrt(sqr(res.dx[point]) + sqr(res.dy[point])));
  }

  return res;
}

CannyRes Canny(const IntensityMatrix& intensity_matrix, double alpha) {
  auto cur_res = Sobel(intensity_matrix, alpha);

  IntensityMatrix res(cur_res.gradient);

  for (auto point : intensity_matrix.GetListOfIndices()) {
    double ang = atan2(cur_res.dy[point], cur_res.dx[point]);
    double mn = 1000;
    int dir_i = 0, dir_j = 0;
    for (int di = -1; di <= 1; di++) {
      for (int dj = -1; dj <= 1; dj++) {
        if (di == 0 && dj == 0) {
          continue;
        }
        double can = atan2(dj, di);
        if (std::abs(can - ang) < mn) {
          mn = std::abs(can - ang);
          dir_i = di;
          dir_j = dj;
        }
      }
    }

    for (int di = -1; di <= 1; di++) {
      for (int dj = -1; dj <= 1; dj++) {
        if (di == 0 && dj == 0) {
          continue;
        }
        if (di * dir_i + dj * dir_j == 0) {
          if (point.row + di < 0 || point.row + di >= intensity_matrix.GetHeight()) {
            continue;
          }
          if (point.column + dj < 0 || point.column + dj >= intensity_matrix.GetWidth()) {
            continue;
          }
          if (cur_res.gradient[point.row + di][point.column + dj] > cur_res.gradient[point]) {
            res[point] = 0;
          }
        }
      }
    }
  }

  CannyRes resp;
  resp.gradient = res;

  return resp;
}

KirschRes Kirsch(const IntensityMatrix& intensity_matrix, double alpha) {
  std::vector<Point> border;
  border.reserve(8);
  for (int i = 0; i < 3; i++) {
    border.emplace_back(Point(0, i));
  }
  for (int i = 1; i < 3; i++) {
    border.emplace_back(Point(i, 2));
  }
  for (int i = 1; i >= 0; i--) {
    border.emplace_back(Point(2, i));
  }
  border.emplace_back(Point(1, 0));
  IntensityMatrix res(intensity_matrix.GetHeight(), intensity_matrix.GetWidth());
  for (int i = 0; i < 8; i++) {
    ConvolutionKernel kernel(3, 3);
    for (auto point : kernel.GetListOfIndices()) {
      kernel[point] = -3;
    }
    kernel[1][1] = 0;
    for (int add = 0; add < 3; add++) {
      int ti = (i + add) % 8;
      kernel[border[ti]] = 5;
    }
    auto cur_res = ApplyConvolution(intensity_matrix, kernel);
    for (auto point : intensity_matrix.GetListOfIndices()) {
      res[point] = std::max(res[point], cur_res[point]);
    }
  }
  for (auto point : res.GetListOfIndices()) {
    res[point] = static_cast<int>(res[point] * alpha);
  }

  KirschRes resp;
  resp.gradient = res;

  return resp;
}

template <typename ThresholdCalculator>
void BuildBinarization(const IntensityMatrix& intensity_matrix,
                       const IndicesList& indices,
                       int depth,
                       ThresholdCalculator calculator,
                       RgbImage* res_image) {
  if (indices.empty() || depth == 0) {
    return;
  }
  assert(depth >= 1);
  int threshold = calculator(intensity_matrix, indices);
  IndicesList blacks, whites;
  GetDivision(intensity_matrix, indices, threshold, &blacks, &whites);
  for (auto point : whites) {
    (*res_image)[point] = kWhitePixel;
  }
  for (auto point : blacks) {
    (*res_image)[point] = kBlackPixel;
  }
  BuildBinarization(intensity_matrix, blacks, depth - 1, calculator, res_image);
  BuildBinarization(intensity_matrix, whites, depth - 1, calculator, res_image);
}

void Flip(RgbImage* matrix) {
  for (auto point : matrix->GetListOfIndices()) {
    matrix->operator[](point) = kWhitePixel - matrix->operator[](point);
  }
}

template <typename ThresholdCalculator>
RgbImage BinarizeImage(const RgbImage& image, ThresholdCalculator calculator, int depth) {
  IntensityMatrix matrix = GrayScale(image);
  RgbImage res_image(image.GetHeight(), image.GetWidth());
  BuildBinarization(matrix, image.GetListOfIndices(), depth, calculator, &res_image);
  return res_image;
}

template <typename ImageOperator, typename ThresholdCalculator>
RgbImage SimpleEdgeDetection(const RgbImage& image,
                             ImageOperator image_operator,
                             ThresholdCalculator calculator,
                             double alpha,
                             bool blur) {
  RgbImage res(image.GetHeight(), image.GetWidth());
  IntensityMatrix matrix = GrayScale(image);
  if (blur) {
    matrix = ApplyGaussBlur(matrix);
  }
  BuildBinarization(image_operator(matrix, alpha).gradient, matrix.GetListOfIndices(), 1, calculator, &res);
  Flip(&res);
  return res;
}

}  // namespace img
