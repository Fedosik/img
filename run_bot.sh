#!/usr/bin/env bash

sudo python3 build_module.py install

export TOKEN=$1
if [[ -z $TOKEN ]]; then
  echo "TOKEN should be specified"
  exit 1
fi

python3 tg_bot.py