from PIL import Image
from ImgModule import *
import io

def GetImage(path):
  try:
    img = Image.open(path)
  except IOError:
    return None
  else:
    return img


def GetImageFromBytes(bytes):
  try:
    img = Image.open(io.BytesIO(bytes))
  except IOError:
    return None
  else:
    return img


def ConvertFromImage(image):
  pix = []
  n, m = image.size
  pixels = image.load()
  for row in range(n):
    for column in range(m):
      pix.append((pixels[row, column]))
  return (n, m, pix)


def ConvertToImage(n, m, p):
  img = Image.new('RGB', (n, m))
  pixels = img.load()
  for i in range(n):
    for j in range(m):
      pixels[i, j] = p[i * m + j]
  return img


def Otsu(image, depth=1):
  n, m, p = ConvertFromImage(image)
  return ConvertToImage(n, m, OtsuBinarize(n, m, p, depth))


def Otsu2d(image, depth=1):
  n, m, p = ConvertFromImage(image)
  print("wtf")
  return ConvertToImage(n, m, Otsu2dBinarize(n, m, p, depth))


def Kirsch(image, blur=True, scalar=1.):
  n, m, p = ConvertFromImage(image)
  return ConvertToImage(n, m, KirschEdgeDetection(n, m, p, blur, scalar))


def Sobel(image, blur=True, scalar=1.):
  n, m, p = ConvertFromImage(image)
  return ConvertToImage(n, m, SobelEdgeDetection(n, m, p, blur, scalar))


def Canny(image, blur=True, scalar=1.):
  n, m, p = ConvertFromImage(image)
  return ConvertToImage(n, m, CannyEdgeDetection(n, m, p, blur, scalar))
