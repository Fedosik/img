from setuptools import setup, Extension

module = Extension('ImgModule', sources=['python_img.cpp'], extra_compile_args=['-std=c++17'])

setup(name='ImgModule',
      version='1.0',
      description='Otsu, Otsu2d, Sobel, Kirsch, Sobel',
      ext_modules=[module])
